<?php
// src/BoxesHUB/BoxBundle/Repository/TransactionRepository.php

namespace BoxesHUB\BoxBundle\Repository;

use Doctrine\ORM\EntityRepository;

class TransactionRepository extends EntityRepository
{
    public function getBalance($conditions)
    {
        
        $condition = " b.user = " . $conditions['id']." and " ;
        
        if ($conditions['month']!==""){ 
            $condition = $condition." b.exedate > '".$conditions['year']."-".$conditions['month']."-00' and b.exedate < '".$conditions['year']."-".$conditions['month']."-32'";
        }else{
             $condition = $condition." b.exedate > '".$conditions['year']."-01-00' and b.exedate < '".$conditions['year']."-12-32'";            
        }
        
                    
        $qb = $this->createQueryBuilder('b')
                   ->select('b')
                   ->where($condition)
                   ->OrderBy('b.exedate', 'DESC');

      

        return $qb->getQuery()
                  ->getResult();
    }

public function getCurrentBalance($id)
{

    $condition = "b.user = ".$id;

     $qb = $this->createQueryBuilder('b')
                   ->select('sum(b.amount)')
                   ->where($condition);

     $res = $qb->getQuery()->getResult();

        return $res[0][1];

}

public function updateBalance($id){
    
   // $condition = "b.user = ".$id;
    $qb = $this->createQueryBuilder('b')
                   ->select('b.id, b.amount, b.balance, b.exedate')
                   ->where('b.user = :id')
                   ->setParameter('id', $id)
                   ->OrderBy('b.exedate', 'ASC');
             
     $transactions = $qb->getQuery()->getResult();
     
     $tra_length = count($transactions);
     $balance = 0;
     $sal = "";
     
     for ($k=0; $k < $tra_length ; $k++){
         $balance = $balance + $transactions[$k]['amount'];
         $ub = $this->createQueryBuilder('b')
                    ->update()
                    ->set('b.balance',$balance)
                    ->where('b.id= :id')
                    ->setParameter('id',$transactions[$k]['id']);
         $sal=$sal + $ub->getQuery()->getResult();
     }
     
    return ($balance);
    
}

}