<?php

namespace BoxesHUB\BoxBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TransactionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            
            ->add('amount')
            ->add('usernameregister','hidden')
            ->add('user')
            ->add('operation','hidden')  
            ->add('comment','textarea',array('required'  => false))
            ->add('type','choice', array(
            'choices'  => array(''=>'',
                                'CREDIT' => 'CREDIT', 
                                'DEBIT' => 'DEBIT')))
            ->add('exedate','date', array(
                            'widget' => 'single_text',
                            'format' => 'MM/dd/yyyy'))
                
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BoxesHUB\BoxBundle\Form\Model\TransactionDefault'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'boxeshub_boxbundle_transaction';
    }
}
