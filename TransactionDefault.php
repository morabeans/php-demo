<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BoxesHUB\BoxBundle\Form\Model;


class TransactionDefault
{
    protected $id;   
    protected $user;
    protected $exedate;
    protected $operation;
    protected $amount;
    protected $usernameregister;
    protected $comment;
    protected $type;
    
    

    public function setUser($user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }
    
    public function setExedate($exedate)
    {
        $this->exedate  = $exedate;
    }

    public function getExedate()
    {
        return $this->exedate;
    }
    
    public function setOperation($operation)
    {
        $this->operation  = $operation;
    }

    public function getOperation()
    {
        return $this->operation;
    }
    
    public function setAmount($amount)
    {
        $this->amount  = $amount;
    }

    public function getAmount()
    {
        return $this->amount;
    }
    
    public function setUsernameregister($usernameregister)
    {
        $this->usernameregister  = $usernameregister;
    }

    public function getUsernameregister()
    {
        return $this->usernameregister;
    }
    
    public function setType($type)
    {
        $this->type  = $type;
    }

    public function getType()
    {
        return $this->type;
    }
    
    public function setComment($comment)
    {
        $this->comment  = $comment;
    }

    public function getComment()
    {
        return $this->comment;
    }
    
    public function setId($id)
    {
        $this->id  = $id;
    }

    public function getId()
    {
        return $this->id;
    }
}