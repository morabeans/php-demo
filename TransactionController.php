<?php

namespace BoxesHUB\BoxBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Session\Session;

use BoxesHUB\BoxBundle\Entity\Transaction;
use BoxesHUB\BoxBundle\Form\TransactionType;
use BoxesHUB\BoxBundle\Form\Model\SearchTransactions;
use BoxesHUB\BoxBundle\Form\SearchTransactionsType;
use BoxesHUB\BoxBundle\Form\Model\TransactionDefault;
use BoxesHUB\BoxBundle\Form\TransactionDefaultType;
use BoxesHUB\BoxBundle\Entity\Operation;
use BoxesHUB\BoxBundle\Entity\Mail;
use BoxesHUB\BoxBundle\Entity\Task;
use BoxesHUB\BoxBundle\Entity\TaskComment;
use BoxesHUB\BoxBundle\Form\CreditType;
use BoxesHUB\BoxBundle\Entity\Credit;

/**
 * Transaction controller.
 *
 * @Route("/boxes")
 */
class TransactionController extends Controller
{
    private $session;
    public function __construct() {
                $this->session = new Session();
    } 
    /**
     * Lists all Transaction entities.
     *
     * @Route("/balance/{id}", name="boxes_balance", defaults={"id" = 0})
     * @Method({"GET", "POST"})
     * @Template()
     */
    
    
    public function indexAction(Request $request,$id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        
        $user = $id;
        if (!$id) {
           $user =$this->container->get('security.context')->getToken()->getUser()->getId();
        }
        
       
        
        $year= date("Y");
        $month ="";
        $conditions = array();
        
        $entity = new SearchTransactions();
        $form = $this->createSearchTransactionsForm($entity);
        $form->handleRequest($request);
        
      
        if ($request->isMethod('POST')) {
            $user_tmp = $entity->getUserId();
            $month_tmp = $entity->getMonth();
            $year_tmp = $entity->getYear();
            if(!empty($user_tmp)){ $user =$entity->getUserId(); }
            if(!empty($month_tmp)){ $month = $entity->getMonth(); }
            if(!empty($year_tmp)){ $year = $entity->getYear(); }
        }else{
            $session_tmp = $this->session->get('userid');
            if(!empty($session_tmp)){
                $user = $this->session->get('userid');
                $this->session->remove('userid');
            }
        }
        
        
        $conditions['id'] = $user;
        $conditions['year'] = $year;
        $conditions['month'] =$month;
        
        $this->session->remove('user_transaction');
        $this->session->set('user_transaction', $user);
        
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('BoxesHUBBoxBundle:Transaction')->getBalance($conditions);
        $user = $em->getRepository('BoxesHUBBoxBundle:User')->findOneBy( array( 'id' => $user));
        
       
       
                 
        return array(
            'entities' => $entities,
            'user_balance' => $user ,
            'form'   => $form->createView(),
        );
    }
    /**
     * Creates a new Transaction entity.
     *
     * @Route("/createtransaction", name="boxes_createtransaction")
     * @Method("POST")
     * @Template("BoxesHUBBoxBundle:Transaction:new.html.twig")
     */
    public function createAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        
       
        $entity = new TransactionDefault();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        
       
        
        if ($form->isValid()) {
           
                       
            $em  = $this->getDoctrine()->getManager();
            $user = $em->getRepository('BoxesHUBBoxBundle:User')->findOneBy( array( 'id' => $entity->getUser()));
            $operation = $em->getRepository('BoxesHUBBoxBundle:Operation')->findOneBy( array( 'id' => $entity->getOperation()));
            $amount= (($entity->getType()!='CREDIT')?($entity->getAmount()*-1):($entity->getAmount()));
            $balance = $amount;
            $date = $entity->getExedate();
            date_time_set($date, date('H'),date('i'),date('s'));
           
            $transaction = new Transaction();
            $transaction->setUser($user);
            $transaction->setType($entity->getType());
            $transaction->setOperation($operation);
            $transaction->setExedate($date);
            $transaction->setAmount($amount);
            $transaction->setComment($entity->getComment());
            $transaction->setUsernameregister($entity->getUsernameregister());
            $transaction->setBalance($balance);
           
            
            // Guarda transaccion
            $em->persist($transaction);
            $em->flush();
            // Actualiza balances
            $balance = $em->getRepository('BoxesHUBBoxBundle:Transaction')->updateBalance( $entity->getUser());
            // Actualiza Balance en el usuario
            $user->setCurrentBalance($balance);
            $em->persist($user);
            $em->flush();
            
            $this->session->set('userid',$entity->getUser());
            return $this->redirect($this->generateUrl('boxes_balance', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Transaction entity.
     *
     * @param Transaction $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(TransactionDefault $entity)
    {
        $form = $this->createForm(new TransactionType(), $entity, array(
            'action' => $this->generateUrl('boxes_createtransaction'),
            'method' => 'POST',
            'attr' => array('id'=>'newtransaction_form')
        ));

        $form->add('button', 'button', array('label' => 'Cancel'));
        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Transaction entity.
     *
     * @Route("/newtransaction", name="boxes_newtransaction")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        $entity = new TransactionDefault();
        $form   = $this->createCreateForm($entity);
        $user = $this->session->get('user_transaction');
                
                

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'user' => $user ,
        );
    }

    /**
     * Finds and displays a Transaction entity.
     *
     * @Route("/showtransaction/{id}", name="boxes_showtransaction")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BoxesHUBBoxBundle:Transaction')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Transaction entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Transaction entity.
     *
     * @Route("/{id}/edittransaction", name="boxes_edittransaction")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BoxesHUBBoxBundle:Transaction')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Transaction entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Transaction entity.
    *
    * @param Transaction $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Transaction $entity)
    {
        $form = $this->createForm(new TransactionType(), $entity, array(
            'action' => $this->generateUrl('boxes_updatetransaction', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Transaction entity.
     *
     * @Route("/updatetransaction/{id}", name="boxes_updatetransaction")
     * @Method("PUT")
     * @Template("BoxesHUBBoxBundle:Transaction:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BoxesHUBBoxBundle:Transaction')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Transaction entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('boxes_edittransaction', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Transaction entity.
     *
     * @Route("/deletetransaction/{id}", name="boxes_deletetransaction")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BoxesHUBBoxBundle:Transaction')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Transaction entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('boxes'));
    }

    /**
     * Creates a form to delete a Transaction entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('boxes_deletetransaction', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
    
    private function createSearchTransactionsForm(SearchTransactions $entity)
    { 
        $form = $this->createForm(new SearchTransactionsType(), $entity, array(
            'action' => $this->generateUrl('boxes_balance', array('id' => $entity->getUserId())),
            'method' => 'POST',
             'attr' => array('id'=>'searchtransactions_form')
        ));

        return $form;
    }
    
    /**
     * Displays a form to create a new Transaction entity.
     *
     * @Route("/buybalance", name="boxes_buybalance")
     * @Method("GET")
     * @Template()
     */
    public function buybalanceAction()
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        
        $message = "";
        $entity = new Credit();
        $form   = $this->createCreateCreditForm($entity);
        $userid = $this->session->get('user_transaction');
        $em   = $this->getDoctrine()->getManager();
        $user = $em->getRepository('BoxesHUBBoxBundle:User')->findOneBy( array( 'id' => $userid)); 
        
        $message = (($this->session->get('message_buycredit'))?($this->session->get('message_buycredit')):(""));
        $this->session->remove('message_buycredit');
                

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'user' => $user ,
            'message' => $message ,
        );
    }
    
     /**
     * Creates a form to create a Transaction entity.
     *
     * @param Transaction $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateCreditForm(Credit $entity)
    {
        $form = $this->createForm(new CreditType(), $entity, array(
            'action' => $this->generateUrl('boxes_createcredit'),
            'method' => 'POST',
            'attr' => array('id'=>'newcredit_form')
        ));

        
        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    
     /**
     * @Route("/taskpaypal", name="boxes_taskpaypal")
     * @Method("GET")
     */
    public function taskpaypalAction()
    {
       
        $userid = $this->session->get('user_transaction');
        $comment = "PayPal: pending buy ".$this->getRequest()->query->get('comment');
        $result = "true";
           try{
            // Crea correo en mailbox del usuario
            $entitydb = new Mail();
                   
            $dbmanager  = $this->getDoctrine()->getManager();
            $user = $dbmanager->getRepository('BoxesHUBBoxBundle:User')->findOneBy( array('id'=>$userid));
            $package = $dbmanager->getRepository('BoxesHUBBoxBundle:Package')->findOneBy( array('name'=>'BOXESHUB INFO'));
            $status = $dbmanager->getRepository('BoxesHUBBoxBundle:ProcessStatus')->findOneBy( array('name'=>'NEW'));
            $subject = $dbmanager->getRepository('BoxesHUBBoxBundle:Subject')->findOneBy( array('name'=>'ACCOUNT INFO'));
              
            $dateset =  new \DateTime(date('Y-m-d H:i:s'));
            $entitydb->setUser($user);
            $entitydb->setPackage($package);
            $entitydb->setStatus($status);
            $entitydb->setSubject($subject);
            $entitydb->setComment($comment);
            $entitydb->setCreatedddate( $dateset);
            $entitydb->setUpdateddate( $dateset);
            $entitydb->setWeight(0);
            $entitydb->setLength(0);
            $entitydb->setHigh(0);
            $entitydb->setDepth(0);
            $entitydb->setUsernameregister("boxesHUB system");
            
           
       
            // Guarda transaccion
            $dbmanager->persist($entitydb);
            $dbmanager->flush();
            
            // Crea tarea para verificar
            
            $taskdb = new Task();
                  
            $status = $dbmanager->getRepository('BoxesHUBBoxBundle:ProcessStatus')->findOneBy( array('name'=>'NEW'));
            $address = $dbmanager->getRepository('BoxesHUBBoxBundle:Address')->findOneBy( array('user'=>$user));
            $command = $dbmanager->getRepository('BoxesHUBBoxBundle:Command')->findOneBy( array('name'=>'Check info'));
            $unpackage = 0;
                  
            //Crear Tarea
           
            $taskdb->setMail($entitydb);
            $taskdb->setCommand($command);
            $taskdb->setStatus($status);
            $taskdb->setAddress($address);
            $taskdb->setUnpackage($unpackage);
            $taskdb->setRequestdate( $dateset);
           
            $dbmanager->persist($taskdb);
            $dbmanager->flush();
                 
            // Guardar comentarios
               
            $taskComment = new TaskComment();

            $taskComment->setTask($taskdb);
            $taskComment->setComment($comment);
            $taskComment->setCommentDate($dateset);
            $taskComment->setUserRegister("boxesHUB system");

            $dbmanager->persist($taskComment);
            $dbmanager->flush();
              
            }catch(\Exception $error){
                
                $result = "false";
            }
           
            $taskdb->addTaskcomment($taskComment);
         if($user->getNotification()){  
               // Envio de la notificacion
            $this->sendNotificationTask($taskdb,$user->getEmail());
            }
            
            $this->sendNotificationTask($taskdb,$this->container->getParameter('sales.email.contact'));
        
        
        return $this->render( 'BoxesHUBBoxBundle:Service:service.html.twig', array(
            'result'         => $result,      
        )  );
    }
    
      /**
     * Creates a new Transaction entity.
     *
     * @Route("/createcredit", name="boxes_createcredit")
     * @Method("POST")
     * @Template("BoxesHUBBoxBundle:Transaction:buybalance.html.twig")
     */
    public function createcreditAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        
       
        $entity = new Credit();
        $form = $this->createCreateCreditForm($entity);
        $form->handleRequest($request);
        $userid = $this->session->get('user_transaction');
        $message = "MSG_01";
       
        
        if ($form->isValid()) {
           
                       
            $dbmanager  = $this->getDoctrine()->getManager();
            $user = $dbmanager->getRepository('BoxesHUBBoxBundle:User')->findOneBy( array( 'id' => $userid));
            $dateset = $entity->getCreditdate();
            
            date_time_set($dateset, date('H'),date('i'),date('s'));
           
           
            $entity->setUser($user);
            $entity->setCreditdate($dateset);
            
            try{
            // Guarda credito
            $dbmanager->persist($entity);
            $dbmanager->flush();
            
             }catch(\Exception $error){
               $message =  "MSG_00";
            }
            // Crea un correo
            
            // Crea correo en mailbox del usuario
            $entitydb = new Mail();
                   
            $dbmanager  = $this->getDoctrine()->getManager();
            $user = $dbmanager->getRepository('BoxesHUBBoxBundle:User')->findOneBy( array('id'=>$userid));
            $package = $dbmanager->getRepository('BoxesHUBBoxBundle:Package')->findOneBy( array('name'=>'BOXESHUB INFO'));
            $status = $dbmanager->getRepository('BoxesHUBBoxBundle:ProcessStatus')->findOneBy( array('name'=>'NEW'));
            $subject = $dbmanager->getRepository('BoxesHUBBoxBundle:Subject')->findOneBy( array('name'=>'ACCOUNT INFO'));
              
            $comment = $entity->getComment();
            $comment = "Transfer by $".$entity->getAmount().". ".((!empty($comment))?($comment):(""));
                 
            
            $entitydb->setUser($user);
            $entitydb->setPackage($package);
            $entitydb->setStatus($status);
            $entitydb->setSubject($subject);
            $entitydb->setComment($comment);
            $entitydb->setCreatedddate( $dateset);
            $entitydb->setUpdateddate( $dateset);
            $entitydb->setWeight(0);
            $entitydb->setLength(0);
            $entitydb->setHigh(0);
            $entitydb->setDepth(0);
            $entitydb->setUsernameregister("boxesHUB system");
            
           
       
            // Guarda transaccion
            $dbmanager->persist($entitydb);
            $dbmanager->flush();
            
            // Crea tarea para verificar
            
            $taskdb = new Task();
                  
            $status = $dbmanager->getRepository('BoxesHUBBoxBundle:ProcessStatus')->findOneBy( array('name'=>'NEW'));
            $address = $dbmanager->getRepository('BoxesHUBBoxBundle:Address')->findOneBy( array('user'=>$user));
            $command = $dbmanager->getRepository('BoxesHUBBoxBundle:Command')->findOneBy( array('name'=>'Check info'));
            $unpackage = 0;
                  
            //Crear Tarea
           
            $taskdb->setMail($entitydb);
            $taskdb->setCommand($command);
            $taskdb->setStatus($status);
            $taskdb->setAddress($address);
            $taskdb->setUnpackage($unpackage);
            $taskdb->setRequestdate( $dateset);
           
            $dbmanager->persist($taskdb);
            $dbmanager->flush();
                 
            // Guardar comentarios
               
            $taskComment = new TaskComment();

            $taskComment->setTask($taskdb);
            $taskComment->setComment($comment);
            $taskComment->setCommentDate($dateset);
            $taskComment->setUserRegister("boxesHUB system");

            $dbmanager->persist($taskComment);
            $dbmanager->flush();
            
            
        }
        
        
         $taskdb->addTaskcomment($taskComment);
         
         if($user->getNotification()){  
               // Envio de la notificacion
            $this->sendNotificationTask($taskdb,$user->getEmail());
            }
            
            $this->sendNotificationTask($taskdb,$this->container->getParameter('sales.email.contact'));
        
        $entity = new Credit();
        $form = $this->createCreateCreditForm($entity);
        
        $this->session->set('message_buycredit', $message);
        
        return $this->redirect($this->generateUrl('boxes_buybalance'));
        
        
    }
    
   private function sendNotificationTask(Task $task, $email)
    {
         
    $subject = " Information about the task: ".$task->getId()." - ".$task->getCommand();
    
      try{
            $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($this->container->getParameter('admin.email.contact'))
            ->setTo($email)->setBody($this->renderView('BoxesHUBBoxBundle:Boss:emailtaskbody.txt.twig', array('task' => $task)));
          
      $this->get('mailer')->send($message);
      $this->addFlash('email', 'Your contact enquiry was successfully sent. Thank you!');
      } catch ( \Exception $error ){
                   
                   
                }
    
    }
}
