<?php

namespace BoxesHUB\BoxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
  * @ORM\Entity(repositoryClass="BoxesHUB\BoxBundle\Repository\TransactionRepository")
  * @ORM\Table(name="transactions")
  * @ORM\HasLifecycleCallbacks()
  */
class Transaction 
{
             
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="exedate", type="datetime" )
     */
    protected $exedate;
     
   /**
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="transactions")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
    
    /**
    * @ORM\Column(type="string", length=32 )
    */
    protected $type;

    
    /**
    /**
     * @ORM\ManyToOne(targetEntity="Operation", inversedBy="transactions")
     * @ORM\JoinColumn(name="operation_id", referencedColumnName="id")
     */
    protected $operation;


    /**
     * @ORM\Column(name="amount", type="float" )
     */
    protected $amount;
    
    /**
     * @ORM\Column(name="balance", type="float" )
     */
    protected $balance;
    
    /**
    * @ORM\Column(type="string", length=45 )
    */
    protected $usernameregister;
    
    /**
    * @ORM\Column(type="string", length=512 , nullable=true )
    */
    protected $comment;

     


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set exedate
     *
     * @param \DateTime $exedate
     * @return Transaction
     */
    public function setExedate($exedate)
    {
        $this->exedate = $exedate;

        return $this;
    }

    /**
     * Get exedate
     *
     * @return \DateTime 
     */
    public function getExedate()
    {
        return $this->exedate;
    }

    /**
     * Set amount
     *
     * @param float $amount
     * @return Transaction
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float 
     */
    public function getAmount()
    {
        return $this->amount;
    }
    
    /**
     * Set amount
     *
     * @param float $balance
     * @return Transaction
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * Get balance
     *
     * @return float 
     */
    public function getBalance()
    {
        return $this->balance;
    }
    
    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Transaction
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Set usernameregister
     *
     * @param string $usernameregister
     * @return Transaction
     */
    public function setUsernameregister($usernameregister)
    {
        $this->usernameregister = $usernameregister;

        return $this;
    }

    /**
     * Get usernameregister
     *
     * @return string 
     */
    public function getUsernameregister()
    {
        return $this->usernameregister;
    }

    /**
     * Set user
     *
     * @param \BoxesHUB\BoxBundle\Entity\User $user
     * @return Transaction
     */
    public function setUser(\BoxesHUB\BoxBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \BoxesHUB\BoxBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Get operation
     *
     * @return string 
     */
    public function getOperation()
    {
        return $this->operation;
    }

    /**
     * Set Operation
     *
     * @param string $operation
     * @return Transaction
     */
    public function setOperation($operation)
    {
        $this->operation = $operation;

        return $this;
    }
    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set Comment
     *
     * @param string $comment
     * @return Transaction
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }
    
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
       
    }
    
       public function __toString() {
        return $this->getOperation();
    }
    
}
